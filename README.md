# ppgen - Passphrase Generator

ppgen generates passphrases from [Ding](https://www-user.tu-chemnitz.de/~fri/ding/) translation files.
This Go implementation is based on Bernhard Reiter's Python 3
[version](http://wald.intevation.org/projects/ppgen/) with some small enhancements.
It's more than twice as fast as the original one.

## Download binary

For some platforms, like windows, you may find binaries attached to the releases details,
see https://gitlab.com/sascha.l.teichmann/ppgen/-/releases .


## Precondition

The generator needs a Ding translation file.  
On a Debian/Ubuntu systems you can install one with

```
# apt-get install trans-de-en
```

Alternatively you can fetch one from [here](http://ftp.tu-chemnitz.de/pub/Local/urz/ding/de-en).
If you embed a ding file (so below) `go generate` will download one for you from there.

## Build

You need a working Go 1.x build environment. Successfully tested with
[Go 1.6](https://golang.org/dl/).

```
$ go get -u -v gitlab.com/sascha.l.teichmann/ppgen
```

Place the resulting `ppgen` binary into your PATH.

You can build `ppgen` with an embedded ding file. Therefore
you have to check it out manually

```
$ git clone https://gitlab.com/sascha.l.teichmann/ppgen.git
$ cd ppgen
$ go generate
$ go build -tags embed
```

A GZIPed ding file will be downloaded and compiled into the binary.

## Usage

    $ ppgen

Picks four words out of the left side of the translation file under
`/usr/share/trans/de-en`. The number of words can be changed with the
parameter `--words=n`. The location of translation file can be
influenced with `--ding=/path/to/ding/file`. The side of the
translation to pick the words from can be set with `-left=value`
with value being `true` or `false`.  
See `ppgen --help` for all options.

## License

(c) 2016 - 2020 by Sascha L. Teichmann

This is free Free Software covered by the terms of the Apache License 2.0.  
See the [LICENSE](https://gitlab.com/sascha.l.teichmann/ppgen/-/blob/master/LICENSE)
file for details.
