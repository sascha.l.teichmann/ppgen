// ppgen
// -----
// A passphrase generator based on words of ding translation file.
//
// (c) 2016 - 2020 by Sascha L. Teichmann <sascha.teichmann@intevation.de>
//
// This is Free Software covered by the terms of the Apache License,
// Version 2.0. See LICENSE file for details.
package main

import (
	"bufio"
	"compress/gzip"
	"crypto/rand"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"math/big"
	"os"
	"path/filepath"
	"runtime"
	"sort"
	"strings"
	"sync"
	"unicode"
	"unicode/utf8"
)

const (
	// How many words to pick from dictionary?
	defaultHowMany = 4
	// Use the left side of the translations?
	defaultUseLeft = true
	// When is a dictionary considered too small?
	tooSmall = 8000
	// Print only the resulting passphrase?
	defaultJustPassphrase = false
	// When using just-passphrase emit white space?
	defaultNoSpace = false
	// Consider only words of pure ASCII chars?
	defaultOnlyASCII = false
)

var Version = "0.1"

// sep is used with  strings.FieldsFunc to split the string.
func sep(c rune) bool {
	return unicode.IsSpace(c) || c == '|' || c == ';' || c == '/'
}

// strip are the unwanted runes in the verbs.
func strip(r rune) bool {
	switch r {
	case '(', '"', ',', '.', ')', '\'', '!', ':', '<', '>':
		return true
	}
	return false
}

// fieldsFunc is similiar to strings.FieldsFunc but it directly calls
// the sep function to separate words and separateted words are
// appended on a reusable slice to reduce alloctions.
func fieldsFunc(s string, a []string) []string {

	fieldStart := -1 // Set to -1 when looking for start of field.
	for i, rune := range s {
		if sep(rune) {
			if fieldStart >= 0 {
				a = append(a, s[fieldStart:i])
				fieldStart = -1
			}
		} else if fieldStart == -1 {
			fieldStart = i
		}
	}
	if fieldStart >= 0 { // Last field might end at EOF.
		a = append(a, s[fieldStart:])
	}
	return a
}

// stats prints out some possibilities based on the size of
// the dictionary and figures out if its maybe too small.
func stats(size int, howMany int, justPassphrase bool) (tainted bool) {

	if !justPassphrase {
		fmt.Printf("Found %d dictionary entries.\n", size)
	}
	if size < tooSmall {
		fmt.Fprintf(os.Stderr,
			"!Your dictionary is below %d entries, that is quite small!\n",
			tooSmall)
		tainted = true
	}

	if size < 1 {
		return
	}

	if !justPassphrase {
		fmt.Println("|= Number of words |= possibilities |")
		for i := 1; i <= howMany; i++ {
			fmt.Printf("|               %2d |    2^%4.1f      |\n",
				i, math.Log2(math.Pow(float64(size), float64(i))))
		}
		fmt.Println()
	}
	return
}

// secureRandInt returns random int from [0, max)
// in a cryptographic secure manner.
func secureRandInt(max *big.Int) *big.Int {
	x, err := rand.Int(rand.Reader, max)
	if err != nil {
		panic(err)
	}
	return x
}

const blockSize = 2048

type blocks chan []string

func (b blocks) alloc() []string {
	select {
	case block := <-b:
		return block
	default:
		return make([]string, blockSize)
	}
}

func (b blocks) free(block []string) {
	select {
	case b <- block[:blockSize]:
	default:
	}
}

func longEnough(word string) bool {
	return utf8.RuneCountInString(word) > 2
}

func onlyASCII(word string) bool {
	for _, r := range word {
		if r > unicode.MaxASCII {
			return false
		}
	}
	return true
}

func acceptASCII(word string) bool {
	return longEnough(word) && onlyASCII(word)
}

// extractWords extracts words from incoming chunks of lines and
// sends them in blocks to the integrating dictionary channel.
func extractWords(
	wg *sync.WaitGroup,
	jobs <-chan []string, extracted chan<- []string,
	accept func(string) bool,
	blks blocks, langIdx int) {

	defer wg.Done()
	words := blks.alloc()
	var wc int
	tmp := make([]string, 0, 64)
	for j := range jobs {
		for _, line := range j {
			if strings.HasPrefix(line, "#") {
				continue
			}
			p := strings.SplitN(line, " :: ", 2)
			if len(p) < 2 {
				continue
			}
			tmp = fieldsFunc(p[langIdx], tmp)
			for _, word := range tmp {
				word = strings.TrimFunc(word, strip)
				if r, w := utf8.DecodeRuneInString(word); w > 0 &&
					r != '[' && r != '{' && accept(word) {
					words[wc] = word
					if wc++; wc == blockSize {
						wc = 0
						extracted <- words
						words = blks.alloc()
					}
				}
			}
			tmp = tmp[:0]
		}
		blks.free(j)
	}
	if wc > 0 {
		extracted <- words[:wc]
	}
}

// readDing extracts howMany words from a ding translation.
// useLeft determines which side of the translation is used.
func readDing(
	r io.Reader, howMany int,
	useLeft, justPassphrase, ascii bool) ([]string, bool, error) {

	var langIdx int
	if !useLeft {
		langIdx = 1
	}

	n := runtime.NumCPU()

	jobs := make(chan []string)
	extracted := make(chan []string, n)

	blks := make(blocks, n*2)

	var accept func(string) bool

	if ascii {
		accept = acceptASCII
	} else {
		accept = longEnough
	}

	var wg sync.WaitGroup
	wg.Add(n)
	for i := 0; i < n; i++ {
		go extractWords(&wg, jobs, extracted, accept, blks, langIdx)
	}

	dictionary := make(map[string]struct{})

	wordsDone := make(chan struct{})
	go func() {
		defer close(wordsDone)
		var counter int
		for ws := range extracted {
			for _, w := range ws {
				dictionary[w] = struct{}{}
				if !justPassphrase {
					if counter++; counter%10000 == 0 {
						fmt.Print(".")
					}
				}
			}
			blks.free(ws)
		}
		if !justPassphrase {
			fmt.Println()
		}
	}()

	s := bufio.NewScanner(r)

	job := blks.alloc()
	var jobCount int
	for s.Scan() {
		line := s.Text()
		job[jobCount] = line
		if jobCount++; jobCount == blockSize {
			jobCount = 0
			jobs <- job
			job = blks.alloc()
		}
	}

	if jobCount > 0 {
		jobs <- job[:jobCount]
	}

	close(jobs)
	wg.Wait()
	close(extracted)
	<-wordsDone

	if err := s.Err(); err != nil {
		return nil, false, err
	}

	tainted := stats(len(dictionary), howMany, justPassphrase)

	if howMany < 1 || len(dictionary) == 0 {
		return nil, tainted, nil
	}

	// To pick the words from the dictionary we
	// generate a respective number of random indices
	// and sort these.
	// Because maps are not random accessible we
	// iterate over the dictionary and extract
	// the words which corresponds to the drawn indices.

	max := big.NewInt(int64(len(dictionary)))
	rndIndices := make([]int, howMany)
	for i := range rndIndices {
		rndIndices[i] = int(secureRandInt(max).Int64())
	}
	sort.Ints(rndIndices)

	words := make([]string, howMany)
	var idx, rndIdx, dupes int
loop:
	for k := range dictionary {
		if idx == rndIndices[rndIdx] {
			for {
				words[rndIdx] = k
				if rndIdx++; rndIdx == howMany {
					break loop
				}
				if rndIndices[rndIdx] > rndIndices[rndIdx-1] {
					break
				}
				dupes++
			}
		}
		idx++
	}

	if dupes > 0 {
		fmt.Fprintf(os.Stderr,
			"! You draw %d duplicates.\n"+
				"! Your random generator is weak\n"+
				"! or you are being very lucky.\n",
			dupes+1)
		tainted = true
	}

	return words, tainted, nil
}

func isGzip(filename string) bool {
	return strings.EqualFold(filepath.Ext(filename), ".gz")
}

// readDingFromFilextracts howMany words from a ding translation file.
// useLeft determines which side of the translation is used.
func readDingFromFile(
	filename string,
	howMany int, useLeft, justPassphrase, ascii bool,
) ([]string, bool, error) {

	f, err := os.Open(filename)
	if err != nil {
		return nil, false, err
	}
	defer f.Close()

	var reader io.Reader

	if isGzip(filename) {
		if r, err := gzip.NewReader(f); err != nil {
			return nil, false, err
		} else {
			reader = r
		}
	} else {
		reader = f
	}

	if !justPassphrase {
		fmt.Printf("Reading entries from %s", filename)
	}
	return readDing(reader, howMany, useLeft, justPassphrase, ascii)
}

func main() {
	var (
		howMany        int
		dingFile       string
		useLeft        bool
		justPassphrase bool
		noSpace        bool
		ascii          bool
		version        bool
	)

	flag.IntVar(&howMany, "words", defaultHowMany, "How many words to select.")
	flag.IntVar(&howMany, "w", defaultHowMany, "How many words to select (shorthand).")
	flag.StringVar(&dingFile, "ding", defaultDingFile, "Path to the ding file.")
	flag.StringVar(&dingFile, "d", defaultDingFile, "Path to the ding file (shorthand).")
	flag.BoolVar(&useLeft, "left", defaultUseLeft, "Use left language (left=false for right).")
	flag.BoolVar(&useLeft, "l", defaultUseLeft, "Use left language (l=false for right) (shorthand).")
	flag.BoolVar(&justPassphrase,
		"just-passphrase", defaultJustPassphrase,
		"Only output the passphrase on a single line.")
	flag.BoolVar(
		&justPassphrase, "j", defaultJustPassphrase,
		"Only output the passphrase on a single line (shorthand).")
	flag.BoolVar(
		&noSpace, "no-space", defaultNoSpace, "passphrase without white space.")
	flag.BoolVar(
		&noSpace, "n", defaultNoSpace, "passphrase without white space (shorthand).")
	flag.BoolVar(
		&ascii, "only-ascii", defaultOnlyASCII, "Only words which consist of ASCII characters only.")
	flag.BoolVar(
		&ascii, "a", defaultOnlyASCII, "Only words which consist of ASCII characters only (shorthand).")
	flag.BoolVar(
		&version, "version", false, "Show the version and exit.")

	flag.Parse()

	if version {
		log.Printf("%s version %s\n", os.Args[0], Version)
		os.Exit(0)
	}

	words, tainted, err := loadDing(dingFile, howMany, useLeft, justPassphrase, ascii)
	if err != nil {
		log.Fatalf("Failed loading ding: %s\n", err)
	}

	if len(words) > 0 {
		if !justPassphrase {
			fmt.Printf("Generated passphrase with %d randomly selected words:\n\n",
				len(words))
		}

		if justPassphrase {
			var join string
			if !noSpace {
				join = " "
			}
			fmt.Println(strings.Join(words, join))
		} else {
			for _, w := range words {
				fmt.Printf("    %s\n", w)
			}
		}
	}

	if tainted {
		fmt.Fprintln(os.Stderr, "!!! Don't use the resulting passphrase !!!")
	}
}
